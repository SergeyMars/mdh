package mdh.server;

import mdh.keyexchange.DH;
import mdh.keyexchange.GOST;
import mdh.keyexchange.SafeStorage;
import mdh.network.Connection;
import mdh.network.ConnectionListener;

import java.io.*;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.*;
import javax.xml.bind.DatatypeConverter;

public class Server implements ConnectionListener {

    Scanner scanner;
    private static DH dh = new DH();
    private static String keysPath = "Side_S/mdhKeys.jks";
    private static Integer filesCount = 0;
    private static SafeStorage ss = new SafeStorage();
    private static GOST gost;

    public static void main(String[] args)
    {
        try {
            checkTerminal();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    private static void checkTerminal() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("\nВведите команду:");
        String userCommandStr = scanner.next();

        if ( (userCommandStr.equals("init")) || (userCommandStr.equals("i")) )
        {
            InitialiseParams();
        }
        else if(userCommandStr.equals("start"))
        {
            System.out.println("\nВведите пароль:");
            String enteredPass = scanner.next();
            ss.getPass(enteredPass);

            new Server();
        }
        checkTerminal();
    }

    private final ArrayList<Connection> connections = new ArrayList<>();

    private Server() {
        System.out.println("Server running");
        try (ServerSocket servSocket = new ServerSocket(3345)) {
            while(true) {
                try {
                    new Connection(this, servSocket.accept());
                } catch (IOException e) {
                    System.out.println("Connection exeption: " + e);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void InitialiseParams() throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Инициализация начата ...");
        System.out.print("Укажите путь к главному носителю  ");
        String pathS = "Side_S"; //scanner.next();
        System.out.print("Укажите путь к контролируемому носителю  ");
        String pathU = "Side_U";//scanner.next();

        System.out.println("\nВведите пароль:");
        String enteredPass = scanner.next();
        ss.getPass(enteredPass);

        dh.InitParams();
        System.out.println("\np = " + dh.p + "\ng = " + dh.g + "\nm = " + dh.m + "\nk = " + dh.k);

        ss.initialisation(dh.p, dh.g, dh.m, dh.k, pathS, pathU);
    }

    @Override
    public synchronized void onConnectionReady(Connection connection) {
        connections.add(connection);

        requestAcceped("Client connected: " + connection);
    }

    @Override
    public synchronized void onReceiveString(Connection connection, String value) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, IOException {
        if (value.equals("REQUEST0")) {
            readRequestAndSendParams(value);
        }
        if (value.equals("REQUEST1")) {
            readAndSend("ACCEPT1");
        }
        else {
            readAndSend(value);
        }
    }

    @Override
    public synchronized void onDisconnect(Connection connection) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, IOException {
        connections.remove(connection);
        readAndSend("Client disconnected: " + connection);
    }

    @Override
    public synchronized void onReceiveBytes(Connection connection, byte[] bValue) { }

    @Override
    public synchronized void onException(Connection connection, Exception e) {
        System.out.println("Connection exeption: " + e);
    }

    private void requestAcceped(String value) {
        System.out.println(value);
        final int cnt = connections.size();
        for (int i=0; i<cnt; i++) {
            connections.get(i).sendString(value);
        }
    }
    private void sendMsg(String value) {
        System.out.println(value);
        final int cnt = connections.size();
        for (int i=0; i<cnt; i++) {
            connections.get(i).sendString(value);
        }
    }

    private void readRequestAndSendParams(String value) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        System.out.println(value);
        final int cnt = connections.size();
        for (int i=0; i<cnt; i++) {
            ss.makeEmptyJKS();
            ss.saveEmptyJKS("Side_S/mdhKeys.jks");

            ss.loadKeystore("Side_S/mdhS.jks");
            String p = ss.getKeyByID("pValue");
            String g = ss.getKeyByID("gValue");

            String folder = "Side_S/";
            String[] files = getFilenames(folder); //{ "Doc1.txt", "Doc2.txt", "Doc3.txt", "Doc4.txt", "Doc5.txt" };
            filesCount = files.length;

            connections.get(i).sendString("ACCEPT0/" + filesCount + "/" + p + "/" + g);
        }
    }

    private void readAndSend(String value) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException, IOException {

        System.out.println(value);
        final int cnt = connections.size();
        for (int i=0; i<cnt; i++) {
            if (value.contains("KEYS/") )
            {
                ss.loadKeystore("Side_S/mdhS.jks");
                String p = ss.getKeyByID("pValue");
                String g = ss.getKeyByID("gValue");
                String m = ss.getKeyByID("mValue");
                String k = ss.getKeyByID("kValue");

                String[] publicUKeys = value.split("/"); // public keys received from U
                Integer numberOfKeys = publicUKeys.length-1;
                BigInteger[] publicSKeys; // generatekeys on S side
                publicSKeys = new BigInteger[numberOfKeys];
                for (Integer kIter=0; kIter<numberOfKeys; kIter++) // k # 1-1 to 5-1
                {
                    dh.setParams(new BigInteger(p), new BigInteger(g), new BigInteger(m), new BigInteger(k), kIter);
                    publicSKeys[kIter] = dh.genPublicKey();
                    ss.saveValue(publicSKeys[kIter].toString(), "k" + kIter.toString(), keysPath);
                }

                BigInteger[] sharedKeys;
                sharedKeys = new BigInteger[numberOfKeys];

                for (Integer kIter=0; kIter<numberOfKeys; kIter++)
                {
                    sharedKeys[kIter] = dh.getSharedKey(publicSKeys[kIter], new BigInteger(publicUKeys[kIter+1]));
                }

                // encrypt Files
                gost = new GOST();
                String folder = "Side_S/";
                String[] files = getFilenames(folder); //{ "Doc1.txt", "Doc2.txt", "Doc3.txt", "Doc4.txt", "Doc5.txt" };
                for (int filesIter = 0; filesIter < numberOfKeys; filesIter++) {
                    System.out.println("shared: " + sharedKeys[filesIter]);
                    String[] splitedPath =  files[filesIter].split("/");
                    gost.encryptFile(folder + splitedPath[splitedPath.length-1], sharedKeys[filesIter]);
                }

                // send Files
                for (int filesIter = 0; filesIter < numberOfKeys; filesIter++) {
                    connections.get(i).sendBytes(files[filesIter] + "enc");
                }

            }
            if (value.contains("SHARED/") ) {
                String k = value.split("/")[1];
                ss.loadKeystore(keysPath);
                String kShared = ss.getKeyByID(k);
                connections.get(i).sendString("SHARED/" + kShared);
            }
            else {
                connections.get(i).sendString(value);
            }
        }
        System.out.println(value);
    }

    public String[] getFilenames(String folderPath) {
        File f = new File(folderPath);
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return !name.endsWith("jks");
            }
        });
        String listOfPaths[] = Arrays.stream(matchingFiles).map(File::getAbsolutePath)
                .toArray(String[]::new);
        Arrays.sort(listOfPaths);
        return listOfPaths;
    }

}