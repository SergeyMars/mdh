package mdh.client;

import mdh.keyexchange.DH;
import mdh.keyexchange.GOST;
import mdh.keyexchange.SafeStorage;
import mdh.network.Connection;
import mdh.network.ConnectionListener;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.*;
import java.util.List;

public class ClientWindow extends JFrame implements ActionListener, ConnectionListener {

    private static DH dh = new DH();
    private static String keysPath = "Side_U/mdhKeys.jks";
    private static SafeStorage ss = new SafeStorage();
    private static GOST gost;
    static final JFrame frame = new JFrame("MDH Exchange");
    private static JButton chooseBtn;
    private static Integer filesCount = 0;
    BigInteger kPrivate;
    BigInteger kPublic;
    BigInteger kShared;

    private static String IP_ADDR = "localhost";
    private static final int PORT = 3345;
    private static final int WIDTH = 600;
    private static final int HEIGHT = 600;
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static int locationX = (screenSize.width - WIDTH) / 2;
    private static int locationY = (screenSize.height - HEIGHT) / 2;

    public ClientWindow() {
    }

    public static void main(String[] args)
    {
//        try { // only localhost
//            getHostIp();
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setBounds(locationX, locationY, WIDTH, HEIGHT);

        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                (new ClientWindow()).ClientWindowGUI(frame.getContentPane());
                // Открытие окна
                frame.pack();
                frame.setVisible(true);

                showPassEnter();
            }
        });
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ClientWindow();
            }
        });
    }

    private static JPanel cards;

    private final JLabel idLabel = new JLabel("ID:");
    private final JTextField idField = new JTextField("MARS");
    private final JLabel log1Label = new JLabel("журнал действий при обмене");
    private JScrollPane scroll1Field = new JScrollPane();
    private final JTextArea log1Field = new JTextArea(12, 30);
    private final JLabel log2Label = new JLabel("журнал действий при расшифровании");
    private JScrollPane scroll2Field = new JScrollPane();
    private final JTextArea log2Field = new JTextArea(12, 30);
    private final JButton requestBtn = new JButton("Соединение");
    private final JButton getkeyBtn = new JButton("Запросить доступ");
    private final JButton openBtn = new JButton("Открыть файл для расшифровки");

    private Connection connection;

    private void ClientWindowGUI(Container container) {
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(Color.BLACK);

        idLabel.setForeground(Color.WHITE);
        // 1
        JPanel idPanel = new JPanel();
        idPanel.setPreferredSize(new Dimension(WIDTH, 30));
        idField.setColumns(20);
        idField.setEditable(false);
        idPanel.add(idLabel);
        idPanel.add(idField);
        idPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        idPanel.setBackground(Color.BLACK);

        // 2
        JPanel btnsPanel = new JPanel();
        btnsPanel.add(requestBtn);
        requestBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getConnection();
            }
        });

        btnsPanel.add(getkeyBtn);
        getkeyBtn.setEnabled(false);
        getkeyBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    showKeysChooser();
                } catch (KeyStoreException keyStoreException) {
                    showAllert("Нет доступа. /nВведен неверный пароль!");
                }
            }
        });
        btnsPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        btnsPanel.setBackground(Color.BLACK);

        log1Label.setAlignmentX(Component.CENTER_ALIGNMENT);
        log1Label.setForeground(Color.WHITE);

        // 3
        JPanel log1Panel = new JPanel();
        scroll1Field = new JScrollPane(log1Field, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        // 4
        JPanel openPanel = new JPanel();
        openPanel.setPreferredSize(new Dimension(WIDTH, 80));
        openBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        openBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    openFileToDecrypt();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        });
        openPanel.add(openBtn);
        openPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        openPanel.setBackground(Color.BLACK);

        log2Label.setAlignmentX(Component.CENTER_ALIGNMENT);
        log2Label.setForeground(Color.WHITE);
        // 5
        JPanel log2Panel = new JPanel();
        log2Panel.setPreferredSize(new Dimension(WIDTH, 100));
        scroll2Field = new JScrollPane(log2Field, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        log2Field.setDragEnabled(true);
        log2Field.setTransferHandler(new TransferHandler(){
            @Override
            public boolean canImport(JComponent comp, DataFlavor[] transferFlavors) {
                return super.canImport(comp, transferFlavors);
            }
        });

        ///
        container.setSize(WIDTH, HEIGHT);
        container.add(idPanel);
        container.add(btnsPanel);
        container.add(log1Label);
        container.add(scroll1Field);
        container.add(openBtn);
        container.add(log2Label);
        container.add(scroll2Field);
        container.setVisible(true);
    }

    // requestBtn cklicked
    public void getConnection()
    {
        try {
            connection = new Connection(this, IP_ADDR, PORT);
            // checkUSB(); try catch
            getkeyBtn.setEnabled(true);

            File f = new File(keysPath);
            if(f.exists() && !f.isDirectory()) {
                printMsg("---------- REQUEST CONNECTION ----------");
                connection.sendString("REQUEST1");
            }
            else {
                printMsg("-------- REQUEST FOR THE FIRST TIME --------");
                connection.sendString("REQUEST0");
            }
        } catch (IOException ex) {
            printMsg("Connection exeption " + ex);

            showAllert("Соединение не установлено.\n" +
                    "Проверьте Интернет соединение \nили попробуйте подключиться позднее");
            getkeyBtn.setEnabled(false);
            getOperatingSystem();
        }
    }

    public void getKey(String aliase)
    {
        if (connection.connected) {
            //checkUSB();
            try {
                ss.loadKeystore(keysPath);
                String k = ss.getKeyByID(aliase);
                kPrivate = new BigInteger(k);

                connection.sendString("SHARED/" + aliase);
                printMsg("------------ Shared key request ------------");
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (UnrecoverableKeyException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
        else {
            showAllert("Соединение не установлено.\n" +
                    "Проверьте Интернет соединение \nили попробуйте подключиться позднее");
        }

    }
    public void openFileToDecrypt() throws IOException {
        JFileChooser fileopen = new JFileChooser();
        int ret = fileopen.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();

            String filePath = file.getPath();
            printLocalMsg("Выбран файл: " + filePath);
            printLocalMsg("Расшифровка начата...");
            gost = new GOST();
            gost.decryptFile(filePath, kShared);
            printLocalMsg("Расшифровка завершена.");
            printLocalMsg("Итоговый файл: " + filePath.substring(0, filePath.length() - 3) );
            showAllert("Расшифровка завершена.");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) { }

    @Override
    public void onConnectionReady(Connection connection) {
        printMsg("Connection ready");
    }

    private static boolean requestAcceped = false;
    @Override
    public void onReceiveString(Connection connection, String value) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        if (value.contains("ACCEPT0/"))
        {
            String[] parts = value.split("/");
            filesCount = Integer.valueOf(parts[1]);
            String p = parts[2];
            String g = parts[3];
            String keyParamsPath = "Side_U/mdhU.jks";
            ss.loadKeystore(keyParamsPath);
            ss.saveValue(p, "pValue", keyParamsPath);
            ss.saveValue(g, "gValue", keyParamsPath);

            String m = ss.getKeyByID("mValue");
            String k = ss.getKeyByID("kValue");

            ss.makeEmptyJKS();
            ss.saveEmptyJKS(keysPath);

            ss.loadKeystore(keysPath);

            String publicKeysStr = "";
            BigInteger[] publicKeys;
            publicKeys = new BigInteger[filesCount];
            for (Integer i = 0; i < filesCount; i++)
            {
                dh.setParams(new BigInteger(p), new BigInteger(g), new BigInteger(m), new BigInteger(k), i);
                publicKeys[i] = dh.genPublicKey();
                ss.saveValue(publicKeys[i].toString(), "k" + i.toString(), keysPath);
                publicKeysStr += "/" + publicKeys[i].toString();
            }
            connection.sendString("KEYS"+publicKeysStr);
        }
        else if (value.contains("ACCEPT1"))
        {
            String keyParamsPath = "Side_U/mdhU.jks";
            ss.loadKeystore(keyParamsPath);
            String p = ss.getKeyByID("pValue");
            String g = ss.getKeyByID("gValue");
            String m = ss.getKeyByID("mValue");
            String k = ss.getKeyByID("kValue");

            dh.setParams(new BigInteger(p), new BigInteger(g), new BigInteger(m), new BigInteger(k), 0);

            printMsg("Соединение установлено.");
        }
        else if (value.contains("FILES/"))
        {
            gost = new GOST();
            try {
                readFiles( value );
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if (value.contains("SHARED/")){
            String[] publicSKey = value.split("/");
            kPublic = new BigInteger(publicSKey[1]);

            kShared = dh.getSharedKey(kPrivate, kPublic);
            showAllert("Ключ получен");
        }

        printMsg(value);

    }

    @Override
    public void onReceiveBytes(Connection connection, byte[] bValue) {
//        try {
//            DataInputStream din = new DataInputStream(connection.socket.getInputStream());
//            String msg = din.readUTF();
//            printMsg(msg);
//            Integer filesCount = din.readInt();
//            printMsg(filesCount.toString());
//
//            for (int j = 0; j < filesCount; j++){

//                long fileSize = din.readLong();
////                String fileName = din.readUTF();
//                FileOutputStream outF = new FileOutputStream("Side_U/test.txt");
//                int count, total = 0;
//
//                while ((count = din.read(buffer, 0, (4096))) != -1){
//                    total += count;
//                    outF.write(buffer, 0, count);
//
//                    if(total == fileSize)
//                        break;
//                }
//
//            }
//            System.out.println("Готово!");
//            connection.socket.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        printMsg(bValue.toString());

    }

    public InputStream stringToInputStream(String initialString) throws IOException {
        return new ByteArrayInputStream(initialString.getBytes());
    }
    public void readFiles(String data) throws IOException {
        String[] text = data.split("/");
        int filesCount = Integer.parseInt(text[1]);
        String filePath = "Side_U/";

        for (int fileIter = 0; fileIter < filesCount; fileIter++){

            long fileSize = Long.parseLong(text[1+2*fileIter]);
            String fileName = text[3+2*fileIter];

            gost.writeFile(filePath + fileName, text[4+2*fileIter]);
            printMsg("Файл " + fileName + " принят.");
        }
        //connection.socket.close();
    }

    @Override
    public void onDisconnect(Connection connection) {
        getkeyBtn.setEnabled(false);
        printMsg("Connection closed");
    }

    @Override
    public void onException(Connection connection, Exception e) {
        printMsg("Connection exeption " + e);
    }

    private synchronized void printMsg(String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log1Field.append(msg + "\n");
                log1Field.setCaretPosition(log1Field.getDocument().getLength());
            }
        });
    }

    private synchronized void printLocalMsg(String msg) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                log2Field.append(msg + "\n");
                log2Field.setCaretPosition(log2Field.getDocument().getLength());
            }
        });
    }

    private static void getHostIp() throws UnknownHostException {
        InetAddress localhost = InetAddress.getLocalHost();
        System.out.println("System IP Address : " +
                (localhost.getHostAddress()).trim());

        // Find public IP address
        String systemipaddress = "";
        try
        {
            URL url_name = new URL("http://bot.whatismyipaddress.com");

            BufferedReader sc =
                    new BufferedReader(new InputStreamReader(url_name.openStream()));

            // reads system IPAddress
            systemipaddress = sc.readLine().trim();
        }
        catch (Exception e)
        {
            systemipaddress = "localhost";
        }
        System.out.println("Public IP Address: " + systemipaddress +"\n");
        IP_ADDR = systemipaddress;
    }

    public String getOperatingSystem() {
        String os = System.getProperty("os.name");
        System.out.println("Using System Property: " + os);
        return os;
    }

    private static void showPassEnter()
    {
        SimpleAttributeSet attrs;
        StyledDocument doc;

//        JFrame f1 = new JFrame("Введите пароль");
        JDialog f1 = new JDialog(frame, "Введите пароль", true);
        f1.setBackground(Color.BLACK);
        f1.setSize(new Dimension(WIDTH/2, HEIGHT/4));

        JTextField area1 = new JTextField();
        area1.setColumns(25);

        JButton okBtn = new JButton("Ok");
        okBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ss.getPass(area1.getText());
                f1.dispose();
            }
        });
        f1.add(area1, BorderLayout.CENTER);
        f1.add(okBtn, BorderLayout.PAGE_END);

        f1.setLocationRelativeTo(null);
        f1.setVisible(true);
    }

    private void showKeysChooser() throws KeyStoreException {
        final String[] keyAliase = {null};
        JDialog f1 = new JDialog(frame, "Выберите ключ", true);
        f1.setBackground(Color.BLACK);
        f1.setSize(new Dimension(WIDTH/2, HEIGHT/2));

        List<String> data = new ArrayList<String>();
        Enumeration<String> enumerationAliases = ss.getAliases(keysPath);
        while (enumerationAliases.hasMoreElements()) {
            data.add( enumerationAliases.nextElement() );
        }
        String[] dataObj = data.toArray(new String[0]);
        String folder = "Side_U/";
        String[] files = getFilenames(folder);
        Arrays.sort(files);
        for (int filesIter = 0; filesIter < dataObj.length; filesIter++) {
            System.out.println(files[filesIter]);
        }

        String[] listItemsText = new String[dataObj.length];
        for (int filesIter = 0; filesIter < dataObj.length; filesIter++) {
            listItemsText[filesIter] = dataObj[filesIter] + " для " + files[filesIter];
        }

        JList list = new JList(listItemsText);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);
        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (e.getValueIsAdjusting() == false) {

                    if (list.getSelectedIndex() == -1) {
                        //No selection, disable fire button.
                        chooseBtn.setEnabled(false);

                    } else {
                        keyAliase[0] = (String) Array.get(dataObj, list.getSelectedIndex());
                        //Selection, enable the fire button.
                        chooseBtn.setEnabled(true);
                    }
                }
            }
        });

        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(250, 80));

        chooseBtn = new JButton(" Запросить ключ");
        chooseBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getKey(keyAliase[0]);
                f1.dispose();
            }
        });
        f1.add(listScroller, BorderLayout.CENTER);
        f1.add(chooseBtn, BorderLayout.PAGE_END);

        f1.setLocationRelativeTo(null);
        f1.setVisible(true);
    }

    public void showAllert(String text)
    {
        SimpleAttributeSet attrs;
        StyledDocument doc;

        JFrame f1 = new JFrame("Предупреждение");
        f1.setBackground(Color.BLACK);
        f1.setSize(new Dimension(WIDTH/2, HEIGHT/3));

        JTextPane area1 = new JTextPane();
        area1.setEditorKit(new MyEditorKit());
        area1.setPreferredSize(new Dimension(WIDTH/2, HEIGHT/5));
        area1.setEditable(false);
        attrs = new SimpleAttributeSet();
        StyleConstants.setAlignment(attrs,StyleConstants.ALIGN_CENTER);
        doc = (StyledDocument)area1.getDocument();
        try {
            doc.insertString(0,text, attrs);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
        doc.setParagraphAttributes(0,doc.getLength()-1,attrs,false);

        JButton okBtn = new JButton("Ok");
        okBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                f1.dispose();
            }
        });
        f1.add(area1, BorderLayout.PAGE_START);
        f1.add(okBtn, BorderLayout.PAGE_END);

        f1.setLocationRelativeTo(null);
        f1.setVisible(true);
    }

    public String[] getFilenames(String folderPath) {
        File f = new File(folderPath);
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith("enc") && !name.endsWith("jks");
            }
        });
        String listOfPaths[] = Arrays.stream(matchingFiles).map(File::getName)
                .toArray(String[]::new);
        return listOfPaths;
    }

    public String[] getFilenames1(String folderPath) {
        File f = new File(folderPath);
        File[] matchingFiles = f.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return !name.endsWith("jks");
            }
        });
        String listOfPaths[] = Arrays.stream(matchingFiles).map(File::getAbsolutePath)
                .toArray(String[]::new);
        return listOfPaths;
    }
}