package mdh.network;

import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.List;

public class Connection {

    public final Socket socket;
    private final Thread rxThread;

    private final OutputStream outStream;
    private final BufferedReader in;
    private final BufferedWriter out;

    private final ConnectionListener eventListener;

    public static boolean connected = false;

    public Connection(ConnectionListener connectionListener, String ipAdress, int port) throws IOException
    {
        this(connectionListener, new Socket(ipAdress, port));
        connected = true;
    }

    public Connection(ConnectionListener connectionListener, Socket socket) throws IOException
    {
        this.eventListener = connectionListener;
        this.socket = socket;

        outStream = socket.getOutputStream();
        in = new BufferedReader( new InputStreamReader(socket.getInputStream(), Charset.forName("UTF-8")) );
        out = new BufferedWriter( new OutputStreamWriter( outStream,  Charset.forName("UTF-8")) );

        rxThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    eventListener.onConnectionReady(Connection.this);
                    while (!rxThread.isInterrupted()) {
                        String msg = in.readLine();
                        try {
                            eventListener.onReceiveString(Connection.this, msg);
                        } catch (UnrecoverableKeyException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (KeyStoreException e) {
                            e.printStackTrace();
                        }
                    }
                    connected = true;

                } catch (IOException e){
                    eventListener.onException(Connection.this, e);
                    connected = false;
                } finally {
                    try {
                        eventListener.onDisconnect(Connection.this);
                    } catch (UnrecoverableKeyException e) {
                        e.printStackTrace();
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (KeyStoreException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        rxThread.start();
    }

    public synchronized void sendString(String value) {
        try {
            out.write(value + "\r\n");
            out.flush(); // очистка буфера
        } catch (IOException e) {
            eventListener.onException(Connection.this, e);
            disconnect();
        }
    }

    public synchronized void sendBytes(String filePath) {
        try{
//            DataOutputStream outD = new DataOutputStream(outStream);
            String data = "";
            data += "FILES/";
            data += (String.valueOf(1));
            data += "/";

//            outD.writeChars("FILES/");
//            outD.writeChars(String.valueOf(1));
//            outD.writeChars("/");
            for (int i = 0; i<1; i++){
                File f = new File(filePath);
                data += String.valueOf(f.length());
                data += "/";
                data += f.getName();
                data += "/";

                FileInputStream in = new FileInputStream(f);
                byte [] buffer = new byte[64*1024];
                int count;

                List<String> lines = Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
                for(String line: lines){
                    data += line;
                }
                data += "/";

            }
            sendString(data);
        }
        catch (Exception e){
            System.err.println(e);
        }
    }

    public synchronized void disconnect() {
        rxThread.interrupt();
        connected = false;
        try {
            socket.close();
        } catch (IOException e) {
            eventListener.onException(Connection.this, e);
        }
    }

    @Override
    public String toString() {
        return "Connection: " + socket.getInetAddress() + "" + ": " + socket.getPort();
    }
}
