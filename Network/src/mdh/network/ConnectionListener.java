package mdh.network;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

public interface ConnectionListener {

    void onConnectionReady(Connection connection);
    void onReceiveString(Connection connection, String value) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, IOException;
    void onDisconnect(Connection connection) throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, IOException;
    void onReceiveBytes(Connection connection, byte[] bValue);
    void onException(Connection connection, Exception e);
}
