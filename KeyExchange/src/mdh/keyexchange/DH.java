package mdh.keyexchange;
import jdk.jshell.EvalException;

import java.security.KeyStore;
import java.util.*;
import java.math.*;
import java.io.*;

/**
 * A class that implements public and private key generation with
 * Diffie-Hellman, and true encryption/decryption via ElGamel.
 */

public class DH
{

    public static BigInteger p;
    public static BigInteger g;

    public static BigInteger m;
    public static BigInteger k;
    public static BigInteger l; // l = k*-m

    public static Integer randSeed = 40;

    public static void main(String argv[]) throws Exception {

        DH dh = new DH();

        BigInteger k1 = new BigInteger("90623761634059982890923707862807397103708650380761829768716070810558491396781257204879569489244192020055355753204869834097921990477708194081041614101824980688648983027296052486522337044302643289388515512100289521623693507410218244011857880741644637303068114006131949540126505358404326201139544878964684234476");
        BigInteger k2 = new BigInteger("94594225137652242773514906187202586016137896552500997882888868441359508149180675404363190733837265723172269415479658266877126391570234752545350114222530027484389980884994980073496196243432331723723317277365054201593318747537626599043557421689970911823686769391798695143988624197778279066713230339546116982620");

        p = new BigInteger("167832029657070040148168985432719994295802326712826573876952769016648470095148171470589290067883230466058601510468149102579666810654285277294609239357578491882762295019466479869891867028130267919374763807781183835627826119583807540132814029994633880157880685352189805230241127271242894358748884374541743703743");
        g = new BigInteger("53");

        m = getM(p);
        k = getM(p);
        BigInteger minusM = getExtendGCD(m, p);
        l = k.multiply(minusM).mod(p);

        dh.getSharedKey(k1, k2);
        dh.getSharedKey(k2, k1);
////////////////////////////////////////////////////////////////
//        // foo INIT_P_G
//        p = BigInteger.probablePrime(1024, new Random(41));
//        g = BigInteger.probablePrime(6, new Random(42));
//
//        m = getM(p);
//        k = getM(p);
//
//        System.out.println("Params: g = " + dh.g + "\n\t p = " + dh.p);
//
//        System.out.println("Params: m = " + dh.m + "\n\t k = " + dh.k);
//
//        BigInteger Kpriv = genPrivateKey(m, k);
//        System.out.println("Kpriv = " + Kpriv);
//
//        BigInteger Kpub = genPublicKey(Kpriv);
//        System.out.println("Kpub = " + Kpub);
//
//        BigInteger Ksess = getSharedKey(Kpriv, Kpub);
//        System.out.println("Ksess = " + Ksess);
    }

    public static void InitParams()
    {
        p = BigInteger.probablePrime(1024, new Random(41));
        g = BigInteger.probablePrime(6, new Random(42));

        m = getM(p);
        k = getM(p);
    }

    public static BigInteger getSharedKey(BigInteger Kpriv, BigInteger Kpub)
    {
        System.out.println("mVAL = " + m);
        System.out.println("pVAL = " + p);
        System.out.println("gVAL = " + g);
        System.out.println("pubVAL = " + Kpub);
        System.out.println("privVAL = " + Kpriv);

        BigInteger numerator1 = power(g, Kpub, p);
        BigInteger numerator = power(numerator1, Kpriv.multiply(m), p);
//        System.out.println("1 = " + numerator);
        BigInteger minusL = getExtendGCD(l, p);
        BigInteger denominator = power(g, minusL, p);
//        System.out.println("l = " + l);
//        System.out.println("(-l) = " + minusL);
//        System.out.println("2 = " + denominator);
        BigInteger Ksess = numerator.multiply(denominator).mod(p); //numerator.divide(denominator).mod(p);
        System.out.println("sharedVAL = " + Ksess);
        return Ksess;
    }

    public static BigInteger power(BigInteger a, BigInteger b, BigInteger n)
    {
        if (b.compareTo(BigInteger.valueOf(0)) == 0) return BigInteger.valueOf(1);
        BigInteger z = power(a, b.divide(BigInteger.valueOf(2)), n);
        if (b.mod(BigInteger.valueOf(2)) == BigInteger.valueOf(0))
            return z.multiply(z).mod(n);
        else
            return a.multiply(z.multiply(z)).mod(n);
    }

    public static void setParams(BigInteger pValue, BigInteger gValue, BigInteger mValue, BigInteger kValue, Integer seed)
    {
        p = pValue;
        g = gValue;

        m = mValue;
        k = kValue;

        BigInteger minusM = getExtendGCD(mValue, p);
        l = kValue.multiply(minusM).mod(p);

        randSeed = 40+seed;
    }

    private static BigInteger genPrivateKey(BigInteger mValue, BigInteger kValue)
    {
        BigInteger Krand = new BigInteger( 128, new Random() ); // randSeed
        BigInteger minusM = getExtendGCD(mValue, p);
        l = kValue.multiply(minusM).mod(p); // mod or not to mod? that is the question
//        System.out.println("k = " + kValue + " \n(-m) = " + minusM + "\nl = " + l);
        BigInteger Kpriv = Krand.add(l).mod(p);
        return Kpriv;
    }

    public static BigInteger genPublicKey()
    {
        BigInteger Kpriv = genPrivateKey(m, k);
        System.out.println("Public!  " + g.modPow( Kpriv, p ));
        return g.modPow( Kpriv, p );
    }

    public static BigInteger getM(final BigInteger max)
    {
        BigInteger M;
        BigInteger  gcdValue;

        do {
            Random rand = new Random();
            M = new BigInteger(1024, rand); // (2^1024-1) is the maximum value
            gcdValue = max.gcd(M);
        }
        while( ( (M.compareTo(p.subtract(BigInteger.valueOf(1))) == 1 ) || M.equals(0) )
                && (gcdValue.compareTo(BigInteger.valueOf(1)) != 0)
        );  // 0< m <p-1 && gcd = 1
        return M;
    }

    private static BigInteger getExtendGCD(BigInteger element, BigInteger basis) {
        BigInteger a = element;
        BigInteger n = basis;

        BigInteger s1 = BigInteger.valueOf(1), s2 = BigInteger.valueOf(0);
        BigInteger t1 = BigInteger.valueOf(0), t2 = BigInteger.valueOf(1);
        while(n.compareTo(BigInteger.valueOf(0)) != 0) {
            BigInteger quotient = a.divide(n); //a / n;
            BigInteger r = a.mod(n); //a % n;
            a = n;
            n = r;
            BigInteger tempS = s1.subtract(s2.multiply(quotient)); //s1 - s2 * quotient;
            s1 = s2;
            s2 = tempS;
            BigInteger tempR = t1.subtract(t2.multiply(quotient)); //t1 - t2 * quotient;
            t1 = t2;
            t2 = tempR;
        }
        if (s1.compareTo(BigInteger.valueOf(0)) <= 0) {
            s1 = s1.add(basis);
        }
        return s1;
    }

}