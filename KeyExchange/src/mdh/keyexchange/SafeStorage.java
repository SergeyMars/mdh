package mdh.keyexchange;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.math.BigInteger;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Scanner;

public class SafeStorage {

    private static char[] pwdArray;
    public static KeyStore ks;

    public static void main(String argv[]) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {

//        ks = null;
//        try {
//            ks = KeyStore.getInstance(KeyStore.getDefaultType());
//        } catch (KeyStoreException e) {
//            e.printStackTrace();
//            System.out.println("Keystore error");
//        }
//
//        getPass();
//
//        String keyPath1 = "Side_S/mdhS.jks";
//        saveValue("320001", "test", keyPath1);
//
//        loadKeystore(keyPath1);
//        String v = getKeyByID("test");
//        System.out.println("Key = " + v);

    }

    public static void initialisation(BigInteger p, BigInteger g, BigInteger m, BigInteger k, String path1, String path2) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {
        makeEmptyJKS();
        String keyPath1 = path1 + "/mdhS.jks";
        saveEmptyJKS(keyPath1);

        saveValue(p.toString(), "pValue", keyPath1);
        saveValue(g.toString(), "gValue", keyPath1);
        saveValue(m.toString(), "mValue", keyPath1);
        saveValue(k.toString(), "kValue", keyPath1);

        makeEmptyJKS();
        String keyPath2 = path2 + "/mdhU.jks";
        saveEmptyJKS(keyPath2);
        saveValue(m.toString(), "mValue", keyPath2);
        saveValue(k.toString(), "kValue", keyPath2);

        System.out.println("Данные успешно записаны");
    }

    public static void makeEmptyJKS()
    {
        ks = null;
        try {
            ks = KeyStore.getInstance(KeyStore.getDefaultType());
        } catch (KeyStoreException e) {
            e.printStackTrace();
            System.out.println("Keystore error");
        }

        // getPass();

        try {
            ks.load(null, pwdArray);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }
    public static void saveEmptyJKS(String keyPath)
    {
        try (FileOutputStream fos = new FileOutputStream(keyPath)) {
            ks.store(fos, pwdArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    public static void saveValue(String valueToSave, String valueID, String keyFile) throws KeyStoreException {
        ks = KeyStore.getInstance("JKS");
        try {
            ks.load(new FileInputStream(keyFile), pwdArray);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }

        ///
        KeyStore.ProtectionParameter protectionParam = new KeyStore.PasswordProtection(pwdArray);
        SecretKey secretValue = new SecretKeySpec(new String(valueToSave).getBytes(), "RSA");
        KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(secretValue);
        ks.setEntry(valueID, secretKeyEntry, protectionParam);
        ///

        try (FileOutputStream fos = new FileOutputStream(keyFile)) {
            ks.store(fos, pwdArray);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }
    }

    public static void getPass(String pswd)
    {
        pwdArray = pswd.toCharArray();
    }

    public static void loadKeystore(String keyFile) throws KeyStoreException {
        ks = KeyStore.getInstance("JKS");
        try {
            ks.load(new FileInputStream(keyFile), pwdArray);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    public static Enumeration<String> getAliases(String keyPath) {
        KeyStore sr = null;
        InputStream is = null;
        Enumeration<String> enumerateAliases = null;
        try {
            sr = KeyStore.getInstance("JKS");
            try {
                is = new FileInputStream(keyPath);
                // intializing keystore object
                try {
                    sr.load(is, pwdArray);
                    enumerateAliases = sr.aliases();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (CertificateException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        return enumerateAliases;
    }

    public static String getKeyByID(String keyID) throws KeyStoreException, UnrecoverableKeyException, NoSuchAlgorithmException {

        if (ks.containsAlias(keyID))
        {
            SecretKey KbyID = (SecretKey) ks.getKey(keyID, pwdArray);
            String secretAsHex = new BigInteger(1, KbyID.getEncoded()).toString(16);
            System.out.println("HEX " + secretAsHex);
            String K = hexToAscii(secretAsHex); //toBaseString(KbyID);
            return K;
        }
        return "0";
    }

    private static String hexToAscii(String hexStr) {
        StringBuilder output = new StringBuilder("");

        for (int i = 0; i < hexStr.length(); i += 2) {
            String str = hexStr.substring(i, i + 2);
            output.append((char) Integer.parseInt(str, 16));
        }
        return output.toString();
    }

    private static void saveKey(String secretKey, String keyID) throws KeyStoreException {
        KeyStore.ProtectionParameter protectionParam = new KeyStore.PasswordProtection(pwdArray);
        SecretKey mySecretKey = new SecretKeySpec(new String(secretKey).getBytes(), "RSA");
        KeyStore.SecretKeyEntry secretKeyEntry = new KeyStore.SecretKeyEntry(mySecretKey);
        ks.setEntry(keyID, secretKeyEntry, protectionParam);
    }

    private static SecretKey toSecretKey(BigInteger key)
    {
        String keyStr = key.toString();
        byte[] decodedKey = Base64.getDecoder().decode(keyStr);
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
        return originalKey;
    }

    private static String toBaseString(Key key)
    {
        String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
        return encodedKey;
    }
}
